var canvas, canvasE, drawLine, drawing, x, y, btn_add_robot, pasteImageRobot,
  erasing, pasteImageDirt, dataModel, radarMove, btn_robot_move, destX, destY, robotMove;

// Инициализующая функция, срабатывает, когда дом готов
function init() {
  canvasE = document.getElementById('example');
  canvas = canvasE.getContext('2d');
  canvasE.addEventListener("mousedown", onMouseDown, false);
  canvasE.addEventListener("mouseup", onMouseUp, false);
  canvasE.addEventListener("click", onClick, false);
  document.getElementById('btn_clear').addEventListener("click", clear, false);
  document.getElementById('btn_eraser').addEventListener('click', startErase, false);
  document.getElementById('btn_add_dirt').addEventListener('click', addDirt, false);
  document.getElementById('btn_create_line').addEventListener('click', createLine, false);
  btn_add_robot = document.getElementById('btn_add_robot');
  btn_add_robot.addEventListener('click', addRobot, false);
  btn_robot_move = document.getElementById('btn_robot_move');
  btn_robot_move.addEventListener('click', letsMove, false);
  dataModel = {};
  dataModel.imgDirt = new Image();
  dataModel.imgDirt.src = "img/dirt.png";
  dataModel.imgRobot = new Image();
  dataModel.imgRobot.src = "img/robot.png";
  clear();
}

// Фунция очистки модели данных
function resetDataModel() {
  dataModel.lines = [];
  dataModel.robot = {};
  dataModel.dirts = [];
  dataModel.dirtLines = [];
  dataModel.radar = {
    x: 30,
    y: 30,
    Px: 30,
    Py: 30,
    length: 1000000,
    foolCircle: 0
  };
  dataModel.lineWidth = 5;
  dataModel.strokeStyle = "#000000";
}

// Функция сброса выполняемых операций
function resetOperations() {
  drawLine = false;
  erasing = false;
  radarMove = false;
  robotMove = false;
  pasteImageRobot = false;
  pasteImageDirt = false;
  btn_robot_move.disabled = true;
}

// Очищаем
function clear() {
  btn_add_robot.disabled = false;
  btn_robot_move.disabled = true;
  canvasE.style.cursor = 'default';
  resetDataModel();
  resetOperations();
  reDraw();
}

// Функция для события мыши, когда нажата кнопка
// Нужна для рисования "стен"
function onMouseDown(e) {
  if (drawLine) {
    drawing = true;
  }
  x = e.layerX;
  y = e.layerY;
}

// Функция для события мыши, когда нажатая кнопка отжата
// Нужна для рисования "стен"
function onMouseUp(e) {
  if (drawLine) {
    canvas.beginPath();
    canvas.moveTo(x, y);
    if (Math.abs(e.layerX - x) > Math.abs(e.layerY - y)) {
      canvas.lineTo(e.layerX, y);
      dataModel.lines.push({
        x1: x,
        y1: y,
        x2: e.layerX,
        y2: y
      });
    } else {
      canvas.lineTo(x, e.layerY);
      dataModel.lines.push({
        x1: x,
        y1: y,
        x2: x,
        y2: e.layerY
      });
    }
    canvas.stroke();
  }
}

// Функция события мыши - клик
function onClick(e) {
  if (pasteImageDirt) {
    canvas.drawImage(dataModel.imgDirt, e.layerX - 20, e.layerY - 20);
    var dirt = {x: e.layerX - 20, y: e.layerY - 20};
    dataModel.dirts.push(dirt);
    dataModel.dirtLines.push({x1: dirt.x + 20, x2: dirt.x + 20, y1: dirt.y + 15, y2: dirt.y + 25, dirt: dirt},
      {x1: dirt.x + 10, x2: dirt.x + 30, y1: dirt.y + 20, y2: dirt.y + 20, dirt: dirt});
  }
  if (pasteImageRobot) {
    canvas.drawImage(dataModel.imgRobot, e.layerX - 25, e.layerY - 25);
    if (!dataModel.robot) {
      dataModel.robot = {};
    }
    dataModel.robot.x = e.layerX - 25;
    dataModel.robot.y = e.layerY - 25;
    pasteImageRobot = false;
    canvasE.style.cursor = 'default';
    btn_add_robot.disabled = true;
    btn_robot_move.disabled = false;
  }
  if (erasing) {
    erase(e.layerX, e.layerY);
  }
}

// Это для ластика, он может удалить выбранный элемент
function erase(eX, eY) {
  var selLine = {}, line, i = -1, b;
  if (dataModel.lines.length > 0) {
    do {
      i++;
      line = dataModel.lines[i];
      if (line.x1 === line.x2) {
        selLine.x1 = line.x1 - 10;
        selLine.x2 = line.x2 + 10;
        if (line.y1 < line.y2) {
          selLine.y1 = line.y1;
          selLine.y2 = line.y2;
        } else {
          selLine.y2 = line.y1;
          selLine.y1 = line.y2;
        }
      } else {
        selLine.y1 = line.y1 - 10;
        selLine.y2 = line.y2 + 10;
        if (line.x1 < line.x2) {
          selLine.x1 = line.x1;
          selLine.x2 = line.x2;
        } else {
          selLine.x2 = line.x1;
          selLine.x1 = line.x2;
        }
      }
      b = isIn(selLine, {x: eX + 5, y: eY + 5});
    } while (i < dataModel.lines.length - 1 && !b);
    if (b) {
      dataModel.lines.splice(i, 1);
    }
  }
  if (dataModel.dirts.length > 0) {
    i = -1;
    b = false;
    do {
      i++;
      selLine.x1 = dataModel.dirts[i].x - 5;
      selLine.x2 = dataModel.dirts[i].x + 45;
      selLine.y1 = dataModel.dirts[i].y - 5;
      selLine.y2 = dataModel.dirts[i].y + 45;
      b = isIn(selLine, {x: eX + 5, y: eY + 5});
    } while (i < dataModel.dirts.length - 1 && !b);
    if (b) {
      dataModel.dirts.splice(i, 1);
    }
  }
  if (dataModel.robot) {
    selLine.x1 = dataModel.robot.x - 5;
    selLine.x2 = dataModel.robot.x + 55;
    selLine.y1 = dataModel.robot.y - 5;
    selLine.y2 = dataModel.robot.y + 55;
    if (isIn(selLine, {x: eX + 5, y: eY + 5})) {
      btn_add_robot.disabled = false;
      btn_robot_move.disabled = true;
      dataModel.robot = null;
    }
  }
  reDraw();
}

// Главная функция для canvas
// Перерисовывает всю модель данных
function reDraw() {
  canvas.beginPath();
  canvas.clearRect(20, 20, canvasE.width, canvasE.height);
  canvas.fillStyle = "#eeeeee";
  canvas.fillRect(20, 20, canvasE.width, canvasE.height);
  canvas.strokeStyle = dataModel.strokeStyle;
  canvas.lineWidth = dataModel.lineWidth;
  canvas.lineCap = "round";
  dataModel.lines.forEach(function (line) {
    canvas.moveTo(line.x1, line.y1);
    canvas.lineTo(line.x2, line.y2);
  });
  canvas.stroke();
  dataModel.dirts.forEach(function (dirt) {
    canvas.drawImage(dataModel.imgDirt, dirt.x, dirt.y);
  });
  if (dataModel.robot) {
    canvas.drawImage(dataModel.imgRobot, dataModel.robot.x, dataModel.robot.y);
  }
  if (robotMove) {
    drawRadar({x: destX, y: destY});
    setTimeout(function () {
      if (dataModel.robot.x + 25 === destX && dataModel.robot.y + 25 === destY) {
        radarMove = true;
        robotMove = false;
        for (var i = 0; i < dataModel.dirts.length; i++) {
          if (dataModel.dirts[i].x === destX - 20 && dataModel.dirts[i].y === destY - 20) {
            break;
          }
        }
        dataModel.radar.foolCircle = 0;
        dataModel.dirts.splice(i, 1);
        collectCollisionLines();
      } else {
        robotToDirt();
      }
      reDraw();
    }, 8);
  }
  if (radarMove) {
    drawRadar({x: dataModel.radar.Px, y: dataModel.radar.Py});
    setTimeout(function () {
      turnRadar();
      var dirtCol = collisions();
      if (dirtCol !== null) {
        radarMove = false;
        robotMove = true;
        destX = dirtCol.x + 20;
        destY = dirtCol.y + 20;
      }
      reDraw();
    }, 15);
  }
}


// Делаем из точки грязи - отрезки, для обнаружения лидаром
function collectCollisionLines() {
  dataModel.dirtLines = [];
  dataModel.dirts.forEach(function(dirt) {
    dataModel.dirtLines.push({x1: dirt.x + 20, x2: dirt.x + 20, y1: dirt.y + 15, y2: dirt.y + 25, dirt: dirt},
      {x1: dirt.x + 10, x2: dirt.x + 30, y1: dirt.y + 20, y2: dirt.y + 20, dirt: dirt});
  })
}


// функция отрисовки линии радара
function drawRadar(dest) {
  canvas.beginPath();
  canvas.strokeStyle = "#FF0000";
  canvas.lineWidth = 2;
  canvas.moveTo(dataModel.robot.x + 24, dataModel.robot.y + 19);
  canvas.lineTo(dest.x, dest.y);
  canvas.stroke();
  canvas.closePath();
}

// функция перемещения робота к целевой грязи
function robotToDirt() {
  var fRadians, DiffX, DiffY, rX = dataModel.robot.x + 25, rY = dataModel.robot.y + 25;
  if (rX !== destX) {
    fRadians = Math.atan(Math.abs(rY - destY) / Math.abs(rX - destX));
  }
  DiffX = Math.round(Math.abs(Math.cos(fRadians)));
  DiffY = Math.round(Math.abs(Math.sin(fRadians)));

  dataModel.robot.x += rX < destX ? DiffX : -DiffX;
  dataModel.robot.y += rY < destY ? DiffY : -DiffY;
}

// начинаем стирать
function startErase() {
  canvasE.style.cursor = "url('img/eraser.cur'), auto";
  resetOperations();
  erasing = true;
}

// старт отрисовки линий ( стен )
function createLine() {
  canvasE.style.cursor = 'crosshair';
  resetOperations();
  drawLine = true;
  canvas.strokeStyle = "#000000";
  canvas.lineWidth = 5;
  canvas.lineCap = "round";
}

// старт добавления грязи
function addDirt() {
  canvasE.style.cursor = 'move';
  resetOperations();
  pasteImageDirt = true;
}

// старт добавления робота
function addRobot() {
  canvasE.style.cursor = 'move';
  resetOperations();
  pasteImageRobot = true;
}

// старт роботу
function letsMove() {
  if (!radarMove || !robotMove) {
    resetOperations();
    radarMove = true;
    btn_robot_move.disabled = !radarMove || !robotMove;
    reDraw();
  }
}

// функция определения принадлежности точки прямоугольнику
var isIn = function (rect, point) {
  return point.x >= rect.x1 && point.x <= rect.x2 && point.y >= rect.y1 && point.y <= rect.y2
};

// функция поворота радара
function turnRadar() {
  if (30 === dataModel.radar.x && 30 === dataModel.radar.y ) {
    dataModel.radar.foolCircle++;
    if (dataModel.radar.foolCircle === 2) {
      resetOperations();
    }
  }
  if (dataModel.radar.y === 30 && dataModel.radar.x < canvasE.width) {
    dataModel.radar.x += 15;
    return;
  }
  if (dataModel.radar.x === canvasE.width && dataModel.radar.y < canvasE.height) {
    dataModel.radar.y += 10;
    return;
  }
  if (dataModel.radar.y === canvasE.height && dataModel.radar.x > 30) {
    dataModel.radar.x -= 15;
    return;
  }
  if (dataModel.radar.x === 30 && dataModel.radar.y > 30) {
    dataModel.radar.y -= 10;
  }
}

// функция, возвращающая длинну отрезка
function segLength(Ax, Ay, Bx, By) {
  var ac = Math.pow(Ax - Bx, 2),
    bc = Math.pow(Ay - By, 2);
  return Math.round(Math.sqrt(ac + bc));
}

// векторное произведение
function area(a, b, c) {
  return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
}

// простое пересечение отрезков
function intersect_1(a, b, c, d) {
  var t;
  if (a > b) {
    t = a;
    a = b;
    b = t;
  }
  if (c > d) {
    t = c;
    c = d;
    d = t;
  }
  return Math.max(a, c) <= Math.min(b, d);
}

// функция проверяющая коллизии линии радара со стенами и грязью
function collisions() {
  var radar = {
      Ax: dataModel.robot.x + 24,
      Ay: dataModel.robot.y + 19,
      Bx: dataModel.radar.x,
      By: dataModel.radar.y
    }, i1, i2, z1, z2, z3, z4,
    Fx = dataModel.radar.x,
    Fy = dataModel.radar.y,
    colLines = dataModel.lines.concat(dataModel.dirtLines),
    length = 1000000,
    dirt = null;
  colLines.forEach(function (line) {
    z1 = area({x: radar.Ax, y: radar.Ay}, {x: radar.Bx, y: radar.By}, {x: line.x1, y: line.y1});
    z2 = area({x: radar.Ax, y: radar.Ay}, {x: radar.Bx, y: radar.By}, {x: line.x2, y: line.y2});
    z3 = area({x: line.x1, y: line.y1}, {x: line.x2, y: line.y2}, {x: radar.Ax, y: radar.Ay});
    z4 = area({x: line.x1, y: line.y1}, {x: line.x2, y: line.y2}, {x: radar.Bx, y: radar.By});
    z1 = z1 * z2;
    z2 = z3 * z4;
    i1 = intersect_1(radar.Ax, radar.Bx, line.x1, line.x2);
    i2 = intersect_1(radar.Ay, radar.By, line.y1, line.y2);
    if (i1 && i2 && z1 < 0 && z2 < 0) {
      var a1, b1, c1, a2, b2, c2, d, dx, dy, Px, Py;

      a1 = radar.By - radar.Ay;
      b1 = radar.Ax - radar.Bx;
      c1 = -radar.Ax * a1 + radar.Ay * (radar.Bx - radar.Ax);

      a2 = line.y2 - line.y1;
      b2 = line.x1 - line.x2;
      c2 = -line.x1 * a2 + line.y1 * (line.x2 - line.x1);

      d = a1 * b2 - b1 * a2;
      dx = -c1 * b2 + b1 * c2;
      dy = -a1 * c2 + c1 * a2;
      Px = Math.round(dx / d);
      Py = Math.round(dy / d);

      if (length > segLength(radar.Ax, radar.Ay, Px, Py)) {
        length = segLength(radar.Ax, radar.Ay, Px, Py);
        Fx = Px;
        Fy = Py;
        if (line.dirt) {
          dirt = line.dirt;
        }
      }
    }
  });
  dataModel.radar.length = length;
  dataModel.radar.Px = Fx;
  dataModel.radar.Py = Fy;
  return dirt;
}